package com.alex.application.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private final String name;
    private final int ordinal;
    private final boolean watched;

    public Session() {
        this.name = "";
        this.ordinal = 0;
        this.watched = false;
    }

    public Session(String name, int ordinal, boolean watched) {
        this.name = name;
        this.ordinal = ordinal;
        this.watched = watched;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public boolean isWatched() {
        return watched;
    }

    @Override
    public String toString() {
        return "Session{" + "id=" + id + ", name=" + name + "ordinal=" + ordinal + ", watched=" + watched + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        Session session = (Session) o;
        return getId() == session.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
