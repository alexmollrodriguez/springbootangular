package com.alex.application.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Trimester {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    private final String name;
    @OneToMany(cascade = CascadeType.PERSIST)
    private final List<Session> sessions;

    public Trimester() {
        this.name = "";
        this.sessions = new ArrayList<>();
    }

    public Trimester(String name, List<Session> sessions) {
        this.name = name;
        this.sessions = sessions;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    @Override
    public String toString() {
        return "Trimester{" + "id=" + id + ", name=" + name + ", sessions=" + sessions + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trimester)) return false;
        Trimester trimester = (Trimester) o;
        return getId() == trimester.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
