package com.alex.application.controllers;

import com.alex.application.entities.Trimester;
import com.alex.application.repositories.TrimesterRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/v1/trimester")
@CrossOrigin(origins = "http://localhost:4200") //this is used for the angular app domain
public class TrimesterController {

    private final TrimesterRepository trimesterRepository;

    public TrimesterController(TrimesterRepository trimesterRepository) {
        this.trimesterRepository = trimesterRepository;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Trimester> findOne(@PathVariable long id) {
        var t = trimesterRepository.findById(id).orElse(null);
        if(t==null) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Trimester>> findAll() {
        var tlist = StreamSupport
                .stream(trimesterRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        if(tlist.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(tlist, HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<Trimester> addOne(@RequestBody Trimester trimester) {
        var t = trimesterRepository.findById(trimester.getId()).orElse(null);
        if(t==null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        trimesterRepository.save(trimester);
        return new ResponseEntity<>(trimester, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Trimester> deleteOne(@PathVariable long id) {
        var t = trimesterRepository.findById(id).orElse(null);
        if(t==null) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        trimesterRepository.delete(t);
        return new ResponseEntity<>(t, HttpStatus.OK);
    }
}