package com.alex.application;

import com.alex.application.entities.Session;
import com.alex.application.entities.Trimester;
import com.alex.application.repositories.TrimesterRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}*/

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * Used to load a bunch of users on the app bootstrap
	 *
	 * @param trimesterRepository the Trimester repository class
	 * @return args
	 */
	@Bean
	CommandLineRunner init(TrimesterRepository trimesterRepository) {
		return args -> {
			//initialise some mock data
			var trimesters = IntStream.range(0, 3).mapToObj(trimNum -> {
				var sessions = IntStream.range(0, 50)
						.mapToObj(sessionNum -> new Session(
								"Session_name_" + (sessionNum+1),
								sessionNum,
								trimNum==0||(trimNum==1&&sessionNum<=30)))
						.collect(Collectors.toList());
				return new Trimester((trimNum+1) + "º Trimestre", sessions);
			}).collect(Collectors.toList());

			//save the data
			trimesterRepository.saveAll(trimesters);
		};
	}
}