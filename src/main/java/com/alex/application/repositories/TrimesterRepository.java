package com.alex.application.repositories;

import com.alex.application.entities.Trimester;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrimesterRepository extends CrudRepository<Trimester, Long> {
    //some custom methods; may not be used
    List<Trimester> findTop10ByOrderByNameDesc();
    Trimester findTopByName(String name);
    List<Trimester> findByNameLike(String name);
}