import { Component, Input } from '@angular/core';
import { Trimester } from '../models/trimester';
import { TrimesterService } from '../services/trimester.service';
import { Utils } from '../utilities/utils'

@Component({
  selector: 'app-temary',
  templateUrl: './temary.component.html',
  styleUrls: ['./temary.component.css']
})
export class TemaryComponent {
  
  @Input() trimesters: Trimester[] = [];

  constructor(private trimesterService: TrimesterService) { }

  countWatchedSessions(trim: Trimester): number {
    return Utils.countWatchedSessions(trim);
  }

  countTotalSessions(trim: Trimester): number {
    return Utils.countTotalSessions(trim);
  }

  hasPendingSessions(trim: Trimester): boolean {
    return Utils.hasPendingSessions(trim);
  }
}
