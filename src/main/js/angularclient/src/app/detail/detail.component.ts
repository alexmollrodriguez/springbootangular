import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Trimester } from '../models/trimester';
import { Session } from '../models/session';
import { TrimesterService } from '../services/trimester.service';
import { Utils } from '../utilities/utils'
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  
  dialogRef!: MatDialogRef<DialogComponent>;
  trimester!: Trimester;
  
  constructor (
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private trimesterService: TrimesterService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
      this.trimesterService.findOne(parseInt(params.get('id')!, 10)))
    ).subscribe(data => {
      this.trimester = data;
    });
  }

  countWatchedSessions(trim: Trimester): number {
    return Utils.countWatchedSessions(trim);
  }

  countTotalSessions(trim: Trimester): number {
    return Utils.countTotalSessions(trim);
  }

  hasPendingSessions(trim: Trimester): boolean {
    return Utils.hasPendingSessions(trim);
  }

  getLastViewed(trim: Trimester): number {
    return Utils.getLastViewed(trim);
  }

  listClick(session: Session) {
    this.dialogRef = this.dialog.open(DialogComponent, {
      hasBackdrop: true,
      height: '400px',
      width: '800px',
      data: session
    });
  }
}