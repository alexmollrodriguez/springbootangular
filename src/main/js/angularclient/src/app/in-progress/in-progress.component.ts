import { Component, Input, OnInit } from '@angular/core';
import { Trimester } from '../models/trimester';

@Component({
  selector: 'app-in-progress',
  templateUrl: './in-progress.component.html',
  styleUrls: ['./in-progress.component.css']
})
export class InProgressComponent implements OnInit {

  @Input() trimesters: Trimester[] = [];

  constructor() { }

  ngOnInit(): void {
  }
}
