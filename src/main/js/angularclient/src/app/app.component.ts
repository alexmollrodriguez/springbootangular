import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title: string;
  languages: string[];
  courses: string[];

  constructor() {
    this.title = 'Tarjetas';
    this.languages = ["Español", "English"];
    this.courses = ["3º de infantil", "4º de infantil", "5º de infantil"];
  }
}
