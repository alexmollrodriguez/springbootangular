import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Trimester } from '../models/trimester';
import { Session } from '../models/session';
import {Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrimesterService {

  private host: string;
  private trimesterResource: string;

  constructor(private http: HttpClient) {
    this.host = 'http://localhost:8080';
    this.trimesterResource = '/api/v1/trimester';
  }

  public findAll(): Observable<Trimester[]> {
    return this.http.get<Trimester[]>(this.host + this.trimesterResource);
  }

  public findOne(id: number): Observable<Trimester> {
    return this.http.get<Trimester>(this.host + this.trimesterResource + '/' + id);
  }
}
