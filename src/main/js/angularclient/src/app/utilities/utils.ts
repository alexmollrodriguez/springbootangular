import { Trimester } from '../models/trimester';

export class Utils {

  static countWatchedSessions(trim: Trimester): number {
    if(!trim) return 0;
    return trim.sessions.filter(s => s.watched).length;
  }

  static countTotalSessions(trim: Trimester): number {
    if(!trim) return 0;
    return trim.sessions.length;
  }

  static hasPendingSessions(trim: Trimester): boolean {
    if(!trim) return true;
    return this.countWatchedSessions(trim) < this.countTotalSessions(trim);
  }

  static getLastViewed(trim: Trimester): number {
    if(!trim) return 0;
    var session = trim.sessions.filter(s => s.watched).sort((a, b) => (a.id > b.id) ? 1 : -1).pop();
    if(!session) return 0;
    return session.id;
  }

}