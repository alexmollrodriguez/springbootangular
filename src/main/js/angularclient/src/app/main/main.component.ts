import { Component, OnInit } from '@angular/core';
import { Trimester } from '../models/trimester';
import { TrimesterService } from '../services/trimester.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  trimesters: Trimester[] = [];

  constructor(private trimesterService: TrimesterService) { }

  ngOnInit(): void {
    this.getData();  
  }

  getData(): void {
    this.trimesterService.findAll().subscribe(data => {
      this.trimesters = data;
    });    
  }

}
