export class Session {
    id: number;
    name: string;
    ordinal: number;
    watched: boolean;
}