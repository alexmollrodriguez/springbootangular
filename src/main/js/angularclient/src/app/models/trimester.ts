import { Session } from "./session";

export class Trimester {
    id: number;
    name: string;
    sessions: Session[];
}