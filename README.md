# Getting Started

This project contains 2 applications:

* A Spring Boot app (for the back-end)
* An Angular app (for the front-end)

## Dependencies

To install the dependencies whe have to do the following:
```bash
# Back-end part
cd {project_root} # (location of this README)
mvn clean install

# Front-end part
cd {project_root}/src/main/js/angularclient
mvn npm install
```

## Running the app

To run the app whe have to do the following:
```bash
# Back-end part
cd {project_root} # (location of this README)
mvn mvn spring-boot:run # now we can fetch info at http://localhost:8080/api/v1/trimester/

# Front-end part
cd {project_root}/src/main/js/angularclient
mvn ng serve # now we can open the app at http://localhost:4200/
```